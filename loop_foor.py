"""
Loop for

Loop -> Estrutura de repetição.;
For -> Umas dessa estruturas

for item in interavel:
    //execução do loop

Utilizamos loops para interar sobre sequecias sobre valores iteráveis


Exemplos de iteraveis:

Exemplos de iteraveis:
 - string:
        nome = 'Homem de ferro'
 - Lista
    lista = [1, 3, 5, 7, 9]
 - Range
   numeros = range(1, 10)
"""

nome = 'Homem de Ferro'
lista = [1, 3, 5, 7]
numeros = range(1, 10)

# Exemplo de for 1(Iterando sobre uma lista)

for letra in nome:
    print(letra)
    
# Exemplo de for 2 (Iterando sobre uma lista)
# for numero in lista:
#     print(numero)

#range(valor_inicial, valor_final)

for numero in range(1, 10):
    print(numero)

