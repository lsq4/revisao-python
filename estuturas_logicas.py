"""
Estruturas logicas: and(e), or(ou), not(não), is (é)

Operadores unários:
 - not
Operadores Binários:
 - and, or, is

 Para o 'and', ambos os valores precisam ser True
 Para o 'or', um ou outro valor precisa ser True
 para o 'not', o valor do booleano é invertido
"""

ativo = False
logado = False

if not ativo:
    print("Você preciar ativar sua conta. Por favor, cheque o seu e-mail")
else:
    print("Bem-vindo usuário")

print(not True)


